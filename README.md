# README #
Installationsanleitung:


    1. Teil: Installation des Frontends (palor-game-hub-htdocs)

    Zum Installieren des Frontends wird ein Webserver vorrausgesetzt.
    
    Schritt 1:
        Die Inhalte des Ordners "palor-game-hub-htdocs" müssen in den htdocs-Ordner des Webservers plaziert werden.
        Wichtig hierbei ist, dass sich alle Dateien im Root-Verzeichnis des Webservers befinden (z.B Linux: /var/www/html/, Xampp: C:\xampp\htdocs\).
        Ansonsten können einige Dateien nicht alle Ressourcen laden.



    2. Teil: Installation des Backend-Servers (pgh-backend-nodejs)
    
    Um das Backend lauffähig zu machen. Wird eine Installation von NodeJS auf der jeweiligen Platform (Windows, Mac, Linux) vorrausgesetzt.
    Desweiteren benötigt NodeJS noch ein Modul. Dieses kann mit den Befehl "npm install websocket" installiert werden. Wichtig hierbei ist, dass
    dieser Befehl im Hauptverzeichnis des Backends ausgeführt wird. In Standartfall wäre dies der Ordner "pgh-backend-nodejs".
    Der Server kann in einem beliebigen Verzeichnis auf dem Endgerät ausgeführt werden, solange alle Dateien ihre Zugehörigkeiten behalten.

    Schritt 1:
        Zum ausführen des Backend-Servers muss die Datei Controller.js ausgeführt werden. Die lässt sich mit dem Befehl "node Controller.js" machen.