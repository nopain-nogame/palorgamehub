window.addEventListener("load", function(){
    if(this.document.getElementsByClassName("container")) {
        setScreen();
        window.addEventListener("resize", function() {
            setScreen();
        })
    }
})

function setScreen() {
    let sections = document.getElementsByTagName("section");
    for(let sect of sections) {
        if(getWidth() <= 1235) {
            setGrid(1, sect);
        } else if(getWidth() <= 1400) {
            setGrid(3, sect);
        } else {
            setGrid(6, sect);
        }
    }
}

function setGrid(maxCol, superElem) {
    let scaleUp = false;
    for(let row of superElem.querySelectorAll(".row")) {
        let columns = row.querySelectorAll(":scope > .col");
        let elements = columns.length;
        let colPercent = 100/elements;
        if(elements > maxCol || scaleUp == true) {
            colPercent = 100/maxCol;
            if((colPercent * elements) % 100 != 0 || scaleUp == true) {
                colPercent = 100;
                scaleUp = true;
            }
        }
        for (i=0;i<elements;i++) {
            if(superElem.hasAttribute("class")) {
                if(superElem.getAttribute("class") == "newsletter-bereich") {
                    let paras = columns[i].querySelectorAll("p");
                    if(maxCol==1 && paras.length > 0) {
                        columns[i].style.display = "none";
                    } else if(maxCol>1 && paras.length > 0) {
                        columns[i].style.display = "block";
                    }
                }
            }
            columns[i].style.width = colPercent+"%";
        }
    }
}

function getWidth() {
    return Math.max(
      document.body.scrollWidth,
      document.documentElement.scrollWidth,
      document.body.offsetWidth,
      document.documentElement.offsetWidth,
      document.documentElement.clientWidth
    );
  }
  
  function getHeight() {
    return Math.max(
      document.body.scrollHeight,
      document.documentElement.scrollHeight,
      document.body.offsetHeight,
      document.documentElement.offsetHeight,
      document.documentElement.clientHeight
    );
  }